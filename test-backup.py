#!/usr/bin/env python3

# import sys
# def r(amount, n):
#     res = []
#     while n > 0:
#         a = amount // n
#         amount -= a
#         n -= 1
#         res.append(a)
#     print( res)

# def r2(amount, n):
#     res = []
#     for i in range(0, n):
#         res.append((i+1) * amount // n - i * amount // n)
#     print(res)

# r(int(sys.argv[1]),int(sys.argv[2]))
# r2(int(sys.argv[1]),int(sys.argv[2]))
# s()

import asyncio
from collections import defaultdict
import logging
import math
import os
from pathlib import Path
from pprint import pprint
import random
import shutil
import socket
from ssl import SSL_ERROR_SSL
import statistics
import sys
import time
from typing import *
import asyncssh
import yaml
from dask.distributed import Client, SSHCluster, as_completed, wait


def parse_config_file(fname):
    try:
        with open(fname, "r") as fd:
            cfg = yaml.safe_load(fd)
            return cfg
    except:
        print("Could not read settings from", fname)
        raise


def default_settings():
    return {
        # "USERNAME": "pfederl",
        "SHARED_DIR_SIZE": 50 * 1024 ** 2,
        "SHARED_NFILES": 2,
        "SHARED_ROOT": "data/shared",
        "WORKER_DIR_SIZE": 50_000_000_000,
        "WORKER_NFILES": 1000,
        "WORKER_ROOT": "data/own",
        "DIR_PARTS": [
            ["lib1", "lib2"],
            ["master", "branch1", "branch2"],
            ["images", "headers", "sources", "build"],
            ["debug", "release", "spec"]
        ],
        "WRITE_WEIGHT": 100,
        "READ_WEIGHT": 100,
        "MODIFY_WEIGHT": 100,
        "DURATION": 300,
        # "USE_HOSTS": "legacy",
        # "HOST_SETS": {
        #     "legacy": [
        #         "linux04-wc.cpsc.ucalgary.ca",
        #         "linux12-ec.cpsc.ucalgary.ca",
        #         "linux13-wb.cpsc.ucalgary.ca",
        #         "linux02-wa.cpsc.ucalgary.ca"
        #     ]
        # }
    }


def read_hosts(fname):
    try:
        all = Path(fname).read_text()
        lines = all.splitlines()
        lines = [line.strip() for line in lines if len(line) > 0]
        lines = [line for line in lines if not line.startswith('#')]
        return lines
    except Exception as e:
        print("Failed to rad hosts from '{fname}'.")
        raise RuntimeError("Failed to load hosts file") from e


def validate_settings(cfg):
    assert isinstance(cfg["WRITE_WEIGHT"], int) and cfg["WRITE_WEIGHT"] >= 0
    assert isinstance(cfg["READ_WEIGHT"], int) and cfg["READ_WEIGHT"] >= 0
    assert isinstance(cfg["MODIFY_WEIGHT"], int) and cfg["MODIFY_WEIGHT"] >= 0
    assert cfg["WRITE_WEIGHT"] + cfg["READ_WEIGHT"] + cfg["MODIFY_WEIGHT"] > 0


def get_settings(fname):
    curr = default_settings()
    cfg = parse_config_file(fname)
    for key, val in cfg.items():
        if key not in curr:
            print(f"Invalid setting '{key}' in {fname}")
            sys.exit(-1)
        curr[key] = val

    validate_settings(curr)
    return curr


def make_tmp_dir(path):
    '''
    creates a directory at path, with a .tmpdir file in it
    '''
    if Path(path).exists():
        print(f"Error: cannot create temp directory '{path}'")
        print(f"       because it already exists.")
        raise RuntimeError("Failed to create tmp dir.")
    Path(path).mkdir(exist_ok=True, parents=True)
    Path(path+"/.tmpdir").write_text("ok to delete")


def rm_tmp_dir(path):
    '''
    recursively removes a directory at path, but only if it contains
    a .tmpdir file in it
    '''
    if not Path(path).exists():
        return
    if not Path(path).is_dir():
        print(f"Error: cannot remove temp directory '{path}'")
        print(f"       because it is a file.")
        raise RuntimeError("Failed to remove tmp dir.")
    try:
        assert Path(path+"/.tmpdir").read_text() == "ok to delete"
    except:
        print(f"Error: refusing to remove temp directory '{path}'")
        print(f"       because it probably wasn't created by me.")
        print(f"       Remove it by hand if you are sure...")
        raise RuntimeError("Refusing to remove tmp dir.")
    try:
        shutil.rmtree(path)
    except Exception as e:
        print(f"Error: Failed to remove temp directory '{path}'")
        raise RuntimeError("shutil.rmtree failed") from e


class Command:
    READ: str = "READ"
    WRITE: str = "WRITE"
    MODIFY: str = "MODIFY"

    def __init__(self, op: str, *, fname: str = "", size: int = -1) -> None:
        self._op: str = op
        self._fname: str = fname
        self._size: int = size
        self._transferred: int = 0

    @classmethod
    def makeRead(cls, fname: str):
        return cls(cls.READ, fname=fname)

    @classmethod
    def makeWrite(cls, fname: str, size: int):
        return cls(cls.WRITE, fname=fname, size=size)

    @classmethod
    def makeModify(cls, fname: str, size: int):
        return cls(cls.MODIFY, fname=fname, size=size)

    def size(self) -> int:
        return self._size

    def fname(self) -> str:
        return self._fname

    def op(self) -> str:
        return self._op

    def transferred(self) -> int:
        return self._transferred

    def setTransferred(self, amount: int) -> None:
        self._transferred = amount


def get_read_dir_commands(root: str) -> List[Command]:
    res = []
    for p in Path(root).rglob("*.dat"):
        if p.is_file():
            res.append(Command.makeRead(str(p)))
    return res


def gen_paths(dir_parts: List[List[str]]) -> List[str]:
    '''
    Constructs all possible paths in a directory tree.
    The tree is specified via dir_parts. For example, given
      dir_parts = [["a","b"],["1","2","3"]]
    The resulting paths will be:
      a, b, a/1, a/2, a/3, b/1, b/2, b/3
    '''
    res: List[List[str]] = []
    curr: List[List[str]] = [[]]
    for ind in range(len(dir_parts)):
        next = []
        for c in curr:
            for name in dir_parts[ind]:
                next.append(c + [name])
        curr = next
        res.extend(curr)
    return ["/".join(dir) for dir in res]


def get_modify_dir_commands(root: str) -> List[Command]:
    return [Command.makeModify(c.fname(), 4096) for c in get_read_dir_commands(root)]


def reshuffled(lst: List[Any]) -> Iterator[Any]:
    '''
    generator for accessing all elements of lst in random order
    once all elements have been accessed, the list is reshuffled again
    '''
    assert len(lst) > 0
    coll = lst[:]
    while True:
        random.shuffle(coll)
        for item in coll:
            yield item


def sample_lists(tuples):
    shuffles = [(reshuffled(lst), weight)
                for lst, weight in tuples if len(lst) > 0 and weight > 0]
    assert len(shuffles) > 0
    while True:
        samples = []
        for g, weight in shuffles:
            samples.extend([next(g) for _ in range(weight)])
        random.shuffle(samples)
        for sample in samples:
            yield sample


def execute_command_write(cmd: Command) -> int:
    assert cmd.op() == cmd.WRITE
    remaining, fname = cmd.size(), cmd.fname()
    Path(fname).parent.mkdir(exist_ok=True, parents=True)
    buff = random.randbytes(4096)
    mv = memoryview(buff)
    # print(f"writing {common.bytes_to_str(remaining):>10s} to '{fname}'")
    with open(fname, "wb") as fd:
        while remaining > 0:
            nw = fd.write(mv[:min(remaining, len(mv))])
            assert nw > 0
            remaining -= nw
    return cmd.size()


def execute_command_read(cmd: Command) -> int:
    assert cmd.op() == Command.READ
    buff = Path(cmd.fname()).read_bytes()
    # print("read ", len(buff), " bytes")
    return len(buff)


def execute_command_modify(cmd: Command) -> int:
    assert cmd.op() == Command.MODIFY
    fname = cmd.fname()
    fsize = os.path.getsize(fname)
    assert fsize > 0
    buff = random.randbytes(min(cmd.size(), fsize))
    pos = random.randint(0, fsize-len(buff))
    with open(fname, "r+b") as fd:
        fd.seek(pos)
        fd.write(buff)
    # print(f"modified {len(buff)} bytes @{pos}")
    return len(buff)


def execute_command(cmd: Command) -> int:
    if cmd.op() == Command.WRITE:
        return execute_command_write(cmd)
    elif cmd.op() == Command.READ:
        return execute_command_read(cmd)
    elif cmd.op() == Command.MODIFY:
        return execute_command_modify(cmd)
    else:
        raise NotImplementedError("commands other than write")


# def execute_commands(commands):
#     for cmd in commands:
#         # print("Executing", cmd)
#         execute_command(cmd)


# used to build both shared and worker directories
def get_build_dir_commands(root: str, dir_parts, total_bytes: int, n_files: int) -> List[Command]:
    n_files = int(n_files)
    commands: List[Command] = []
    paths = gen_paths(dir_parts)
    paths = [f"{root}/{path}" for path in paths]
    sizes = [random.uniform(10, 12) for i in range(n_files)]
    mysum = sum(sizes)
    sizes = [int(s * total_bytes / mysum) for s in sizes]
    for s, fn in zip(sizes, range(len(sizes))):
        commands.append(Command.makeWrite(
            random.choice(paths) + f"/file{fn}.dat", s))
        # (CMD_WRITE, s, random.choice(paths) + f"/file{fn}.dat"))
    return commands

# formats size to a string with units with apropriate type
# e.g. bytes_to_str(1024) -> "1.00 KB"


def bytes_to_str(size: float) -> str:
    for unit in ["b", "KB", "MB", "GB", "TB"]:
        if size < 1024.0:
            return f"{size:3.2f}{unit}"
        size /= 1024.0

    return f"{size*1024:3.1f} TB"

# Builds a shared directory for all workers to read and/or modify.
# If directory already exists, but it was built with different
# parameters, it will be deleted, otherwise it will be re-used.


def build_dir_cached(root, dir_parts, total_bytes, n_files):
    print(
        f"Building shared directory {root} with "
        + f"{n_files} files and {bytes_to_str(total_bytes)}")
    sig = str([root, dir_parts, total_bytes, n_files])
    completed_file = Path(root) / Path(".complete")
    if completed_file.exists():
        sig2 = completed_file.read_text()
        if sig == sig2:
            print(f"  - skipping, as directory already exists: {root}")
            return
    if Path(root).exists():
        print("  - removing old shared directory")
        rm_tmp_dir(root)
        if Path(root).exists():
            print("Error: failed to remove shared directory.")
            raise RuntimeError("Cannot remove share dir.")
    make_tmp_dir(root)
    commands = get_build_dir_commands(root, dir_parts, total_bytes, n_files)
    last_report_time = time.time()-1
    for ind, cmd in enumerate(commands):
        execute_command(cmd)
        if time.time() - last_report_time > 0.5:
            print(
                f"  - created {ind+1}/{len(commands)} files [{(ind+1)*100/len(commands):.1f}%]       ", end='\r')
            last_report_time = time.time()
    print(f"  - created {len(commands)} files                ")
    completed_file.write_text(sig)


def run_tests(n_workers: int, cwd: str, cfg: dict):

    os.chdir(cwd)

    # return {
    #     "elapsed": time.time(),
    #     "n_writes": 1,
    #     "n_reads": 2,
    #     "n_modifications": 3,
    #     "hostname": socket.gethostname(),
    #     "cfg": cfg,
    #     "cwd": os.getcwd()
    # }

    id = socket.gethostname()
    start_time = time.time()

    write_commands = get_build_dir_commands(
        cfg["WORKER_ROOT"] + f"/{id}",
        cfg["DIR_PARTS"],
        math.ceil(cfg["WORKER_DIR_SIZE"] / n_workers),
        cfg["WORKER_NFILES"] / n_workers)
    read_commands = get_read_dir_commands(cfg["SHARED_ROOT"])
    modify_commands = get_modify_dir_commands(cfg["SHARED_ROOT"])
    shuffler = sample_lists([
        (write_commands, cfg["WRITE_WEIGHT"]),
        (read_commands, cfg["READ_WEIGHT"]),
        (modify_commands, cfg["MODIFY_WEIGHT"])
    ])
    counts: dict = defaultdict(lambda: 0)
    start_time = time.time()
    total_bytes = 0
    while time.time() - start_time < cfg["DURATION"]:
        cmd = next(shuffler)
        counts[cmd.op()] += 1
        # print(cmd)
        total_bytes += execute_command(cmd)

    return {
        "elapsed": time.time() - start_time,
        "n_writes": counts[Command.WRITE],
        "n_reads": counts[Command.READ],
        "n_modifications": counts[Command.MODIFY],
        "hostname": socket.gethostname(),
        "total_bytes": total_bytes
    }


def filter_up_hosts(hosts):
    # print("Removing hosts that are not responding...")
    res = []

    async def check_host(host):
        try:
            # print("Connecting to ", host)
            conn = await asyncssh.connect(host, connect_timeout=5, known_hosts=None)
            # print("  connected to ", host)
            await conn.run("echo", timeout=10)
            res.append(host)
        except:
            print(f"  - no response from {host}")

    async def check_hosts(hosts):
        futures = [check_host(host) for host in hosts]
        return await asyncio.gather(* futures)
    results = asyncio.run(check_hosts(hosts))
    return res


def describe_test(cfg):
    print("Test details:")
    print("  - shared directory:")
    print("     - total size: ", bytes_to_str(cfg["SHARED_DIR_SIZE"]))
    print("     - number of files:", cfg["SHARED_NFILES"])
    print("     - average file size:",
          bytes_to_str(cfg["SHARED_DIR_SIZE"] / cfg["SHARED_NFILES"]))
    shsize = cfg["WORKER_DIR_SIZE"]
    shnfiles = cfg["WORKER_NFILES"]
    print("  - worker directories:")
    print("     - total size up to: ", bytes_to_str(shsize))
    print("     - number of files up to: ", shnfiles)
    print("     - average file size:", bytes_to_str(shsize / shnfiles))
    print("  - duration:", cfg["DURATION"], "s")
    print("  - write weight:", cfg["WRITE_WEIGHT"])
    print("  - read weight:", cfg["READ_WEIGHT"])
    print("  - modify weight:", cfg["MODIFY_WEIGHT"])


def display_results(res, cfg):
    t = Table("Per worker stats")
    t.new_row("=")
    t.add_to_col(0, "Worker").add_to_col(1, "# reads").add_to_col(
        2, "# writes").add_to_col(3, "# modifications").add_to_col(4, "Bytes transferred")
    mods, reads, writes, elapseds, total_bytes = [], [], [], [], []
    t.new_row("=")
    for ind, val in enumerate(res.values()):
        mods.append(val["n_modifications"])
        reads.append(val["n_reads"])
        writes.append(val["n_writes"])
        elapseds.append(val["elapsed"])
        total_bytes.append(val["total_bytes"])
        t.add_to_col(0, ind) \
         .add_to_col(1, str(val["n_reads"])) \
         .add_to_col(2, str(val["n_writes"])) \
         .add_to_col(3, str(val["n_modifications"])) \
         .add_to_col(4, f"{val['total_bytes']:,}")
    t.new_row("=")
    t.add_to_col(0, "Average")
    t.add_to_col(1, f"{statistics.mean(reads):.3f}")
    t.add_to_col(2, f"{statistics.mean(writes):.3f}")
    t.add_to_col(3, f"{statistics.mean(mods):.3f}")
    t.add_to_col(4, f"{int(statistics.mean(total_bytes)):,}")
    t.add_to_col(0, "Median")
    t.add_to_col(1, f"{statistics.median(reads):.3f}")
    t.add_to_col(2, f"{statistics.median(writes):.3f}")
    t.add_to_col(3, f"{statistics.median(mods):.3f}")
    t.add_to_col(4, f"{int(statistics.median(total_bytes)):,}")
    t.new_row("=")
    t.print()

    print()
    print()
    describe_test(cfg)
    total_ops = sum(reads + writes + mods)
    ops_rate = total_ops / statistics.mean(elapseds)
    total_bytes = sum(total_bytes)
    bytes_pr_sec = total_bytes / statistics.mean(elapseds)
    print(f"Workers used:            {len(res.values())}")
    print(f"Total operations:        {total_ops}")
    print(f"Operations per second:   {ops_rate:.3f}")
    print(f"Total bytes transferred: {bytes_to_str(total_bytes)}")
    print(f"Total bytes transferred: {total_bytes:,}")
    print(f"Bytes transfer rate:     {bytes_to_str(bytes_pr_sec)} /sec")


def parse_command_line(argv):
    if len(argv) != 3:
        print("Usage: test.py settings.yml host-list.txt")
        sys.exit(-1)
    return argv[1], argv[2]


def main():

    cfg_fname, hosts_fname = parse_command_line(sys.argv)

    # disable INFO messages from dask:
    dask_logger = logging.getLogger("distributed")
    dask_logger.setLevel(logging.WARN)

    # enable asyncssh logging to stdout - useful for debugging
    if False:
        asyncssh.logging.set_log_level(logging.INFO)
        asyncssh.logging.set_debug_level(2)
        asyncssh_logger = logging.getLogger('asyncssh')
        asyncssh_logger.addHandler(logging.StreamHandler(sys.stdout))

    # obtain settings
    cfg = get_settings(cfg_fname)
    describe_test(cfg)

    # figure out which hosts we'll be using
    hosts = read_hosts(hosts_fname)
    # hosts_set_name = cfg["USE_HOSTS"]
    # if hosts_set_name not in cfg["HOST_SETS"]:
    #     print(f"Cannot find host set '{hosts_set_name}'")
    #     print(f"defined sets are: {', '.join((cfg['HOST_SETS'].keys()))}")
    #     sys.exit(-1)
    # hosts = cfg["HOST_SETS"][hosts_set_name]
    print(f"You specified {len(hosts)} hosts: {hosts[:4]}...")
    print(f"Filtering out hosts that are down...")
    up_hosts = filter_up_hosts(hosts)
    print(f"Removed hosts: {len(hosts) - len(up_hosts)}")

    if len(up_hosts) == 0:
        print("Error: no hosts are actually up. Bye.")
        sys.exit(-1)

    # if cfg["READ_WEIGHT"] > 0 or cfg["MODIFY_WEIGHT"] > 0:
    #     assert cfg["SHARED_NFILES"] >= len(up_hosts)
    if cfg["WRITE_WEIGHT"] > 0:
        assert cfg["WORKER_NFILES"] >= len(up_hosts)
        
    assert len(up_hosts)

    # prepare the shared directory
    build_shared_time = time.time()
    build_dir_cached(
        cfg["SHARED_ROOT"],
        cfg["DIR_PARTS"],
        cfg["SHARED_DIR_SIZE"],
        cfg["SHARED_NFILES"])
    build_shared_time = time.time() - build_shared_time
    print(f"Shared build finished in {build_shared_time:.1f}s")

    # get rid of old worker directories
    print("Removing old worker directories")
    rm_tmp_dir(cfg["WORKER_ROOT"])
    make_tmp_dir(cfg["WORKER_ROOT"])
    print("  - done")

    # run_tests(len(up_hosts), os.getcwd(), cfg)
    res = run_tests(len(up_hosts), os.getcwd(), cfg)
    res = {"1.1.1.1": res}
    pprint(res)
    display_results(res, cfg)
    return

    # start up cluster
    print("Starting up cluster")
    cluster = SSHCluster(
        ["localhost"] + up_hosts,
        connect_options={
            "known_hosts": None,
            "connect_timeout": 15
        },
        worker_options={"nthreads": 1, "local_directory": "/tmp"},
        scheduler_options={"port": 0, "dashboard_address": ":8797"},
    )
    client = Client(cluster)
    print("Cluster is up")
    print(client)
    workers = client.scheduler_info()['workers']
    n_workers = len(workers)
    print(f"Number of workers running: {n_workers}")
    if n_workers != len(hosts):
        print("===========================================================")
        print("Warning: you specified more hosts, but some")
        print("of them could not run workers.")
        print("Check info above to see why this might be happening...")
        print("===========================================================")

    # print( "Testing uniqueness of hosts...")
    # futures = client.map(lambda _ : socket.gethostname(), hosts)
    # wait(futures)
    # print([f.result() for f in futures])

    print(f"Running tests on all workers for {cfg['DURATION']}s")
    results = client.run(run_tests, n_workers, os.getcwd(), cfg, callback_timeout=3600)
    print("Tests finished")
    pprint(results)
    # futures = client.map(run_tests, [cfg] * len(hosts))
    # wait(futures)
    # pprint([f.result() for f in futures])
    print()
    print()
    display_results(results, cfg)
    return


class Table:
    def __init__(self, title=None, max_col_width=None) -> None:
        self.rows = []
        self.curr_row = {}
        self.max_widths = {}
        self.n_cols = 0
        self.col_align = {}
        self.default_max_col_width = max_col_width
        self.max_col_widths = {}
        self.title = title

    def get_max_col_width(self, col):
        return self.max_col_widths.get(col, self.default_max_col_width)

    def set_max_col_width(self, col, width):
        self.max_col_widths[col] = width

    def new_row(self, sep: str = None):
        assert sep is None or len(sep) == 1
        if len(self.curr_row) > 0:
            self.rows.append(self.curr_row)
        self.curr_row = {}
        if sep is not None:
            self.rows.append(sep)
        return self

    # txt can be:
    #   - string with new lines
    #   - array of items that can be converted so str with str()
    #   - int or float
    # any other item will be converted to [str(txt)]
    def add_to_col(self, column: int, content):
        if isinstance(content, int) or isinstance(content, float):
            content = [str(content)]
        elif isinstance(content, list):
            content = [str(x) for x in content]
        elif isinstance(content, str):
            content = content.splitlines()
        else:
            content = str(content)
        lines = content[:]
        # update number of columns
        self.n_cols = max(self.n_cols, column + 1)
        # make sure entry for column exists
        self.curr_row[column] = self.curr_row.get(column, [])
        # make all lines shorter
        if lines == []:
            lines = [""]
        shortlines = []
        mcw = self.get_max_col_width(column)
        for line in lines:
            while mcw is not None and len(line) > mcw:
                shortlines.append(line[:mcw])
                line = line[mcw:]
            shortlines.append(line)
        # add each short line to the column
        for line in shortlines:
            self.curr_row[column].append(line)
            self.max_widths[column] = max(
                self.max_widths.get(column, 0), len(line))
        return self

    def set_col_align(self, col, align="left"):
        assert align in ["left", "right", "center"]
        if isinstance(col, list):
            for c in col:
                self.col_align[c] = align
        else:
            self.col_align[col] = align
        return self

    def print(self):
        print(self.to_str())

    def to_lines(self):
        self.new_row()
        res = []
        for row in self.rows:
            res.append(self.row_to_str(row))
        return res

    def to_str(self):
        lines = self.to_lines()
        if self.title is not None:
            lines = ["=" * len(self.row_to_str("=")), self.title] + lines
            # lines = ["=" * len(lines[0]), self.title] + lines
        return "\n".join(lines)

    def row_to_str(self, row):
        if isinstance(row, str):
            res = ""
            for col in range(self.n_cols):
                self.max_widths[col] = self.max_widths.get(col, 0)
                res += row * (2 + self.max_widths[col])
                if col < self.n_cols - 1:
                    res += "+"
            return res
        n_sub_rows = 0
        for c in range(self.n_cols):
            row[c] = row.get(c, [])
            n_sub_rows = max(n_sub_rows, len(row[c]))
        res = ""
        for sr in range(n_sub_rows):
            for col in range(self.n_cols):
                self.max_widths[col] = self.max_widths.get(col, 0)
                align = self.col_align.get(col, "left")
                if sr < len(row[col]):
                    subrow = row[col][sr]
                else:
                    subrow = ""
                assert len(
                    subrow) <= self.max_widths[col], f"subrow={subrow}, col={col}"
                pad = self.max_widths[col] - len(subrow)
                if align == "left":
                    res += " " + subrow + " " + (" " * pad)
                elif align == "right":
                    res += (" " * pad) + " " + subrow + " "
                else:
                    res += (" " * (pad // 2)) + " " + subrow + \
                        " " + (" " * (pad - pad // 2))
                if col < self.n_cols - 1:
                    res += "|"
            if sr < n_sub_rows - 1:
                res += "\n"
        return res


if __name__ == "__main__":
    main()
